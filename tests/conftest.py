import logging
import logging.config

import pytest


def pytest_html_report_title(report):
    report.title = "Test results"


@pytest.fixture(scope="session")
def fake_logger():
    fake_conf_dict = {
        "version": 1,
        "disable_existing_loggers": True,
        "formatters": {
            "standard": {
                "format": "time=%(asctime)s.%(msecs)03dZ|level_name=%(levelname)s|logger_name=%(name)s|path_name=%(pathname)s|file_name=%(filename)s|func_name=%(funcName)s|lineno=%(lineno)d|log_message=%(message)s",
                "datefmt": "%Y-%m-%dT%H:%M:%S",
            }
        },
        "handlers": {
            "file_handler": {
                "class": "logging.handlers.RotatingFileHandler",
                "level": "INFO",
                "formatter": "standard",
                "filename": "/tmp/spark_logs.log",
                "mode": "a",
            },
            "stream_handler": {
                "class": "logging.StreamHandler",
                "level": "WARNING",
                "formatter": "standard",
                "stream": "ext://sys.stdout",
            },
        },
        "root": {"level": "NOTSET", "handlers": ["stream_handler"]},
        "loggers": {"applicatif": {"level": "INFO", "handlers": ["file_handler", "stream_handler"], "propogate": True}},
    }
    logging.config.dictConfig(config=fake_conf_dict)
    return logging.getLogger()
