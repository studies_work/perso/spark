import logging
from contextlib import nullcontext as does_not_raise

import pytest
from freezegun import freeze_time

from src.infrastructure.logs import Logger
from src.infrastructure.read_conf import get_conf


@pytest.fixture(scope="function")
def tmp_file_path(tmp_path):
    return tmp_path / "logs.log"


def test_type():
    logger = Logger().get_logger()
    assert isinstance(logger, logging.Logger)


@pytest.mark.parametrize(
    "log_level_int, log_level_str, log_name, log_message, expectation",
    [
        (
            10,
            "DEBUG",
            "applicatif",
            "This is a DEBUG error, test should fail as the set level is INFO",
            pytest.raises(ValueError),
        ),
        (20, "INFO", "applicatif", "This is a INFO error", does_not_raise()),
        (
            30,
            "WARNING",
            "",
            "This is a WARNING error, test should fail as their is only stream_handler for root logger",
            pytest.raises(ValueError),
        ),
        (30, "WARNING", "applicatif", "This is a WARNING error", does_not_raise()),
    ],
)
@freeze_time("2023-12-31 00:00:00", tz_offset=+2)
def test_get_logger(log_level_int, log_level_str, log_name, log_message, expectation, tmp_file_path):
    config = get_conf(conf_file_name="log_conf")
    config["handlers"]["file_handler"]["filename"] = tmp_file_path
    logger = Logger(config_dict=config).get_logger(logger_name=log_name)
    logger.log(log_level_int, log_message)
    with open(tmp_file_path, "r") as f:
        with expectation:
            output = f.read()
            output_dict = dict(item.split("=") for item in output.split("\n")[0].split("|"))
            assert output_dict["time"] == "2023-12-31T00:00:00.000Z"
            assert (output_dict["level_name"] == log_level_str) & (output_dict["log_message"] == log_message)
