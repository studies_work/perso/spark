PYTHON_VERSION ?= 3.11.9
POETRY_VERSION ?= 1.8.3

export PYTHONPATH := $(CURDIR)

install-python: ## 🐍️ install python with pyenv
	@pyenv install --skip-existing $(PYTHON_VERSION)
	@pyenv local $(PYTHON_VERSION)

install-poetry: ## ✅ check if poetry is present and install it if not
	@if [ "$(shell which poetry)" = "" ]; then pip install poetry==$(POETRY_VERSION); fi

install-dependencies: pyproject.toml ## 🛠️install project dependencies
	@poetry --version
	@poetry config virtualenvs.prefer-active-python true
	@poetry config virtualenvs.in-project true
	@poetry install --with dev
	@poetry shell

global-setup: ## 🌐 perform several installation
	@make install-python
	@make install-poetry
	@make install-dependencies

coverage: ## 🎯 run coverage with report creation
	@poetry run pytest --cov=./ tests/ --cov-report=html --cov-append --suppress-tests-failed-exit-code

test: ## 🎯 run tests with report creation
	@poetry run pytest --html="htmlcov/test_results_$$(date +"%Y_%m_%_d_%H_%M_%S").html" --self-contained-html --suppress-tests-failed-exit-code

clean: ## 🧹 clean up project
	@rm -rf .venv
	@rm -rf .pytest_cache
	@rm -rf .python-version
	@rm -rf .coverage
	@find . | grep -E "(__pycache__)" | xargs rm -rf
