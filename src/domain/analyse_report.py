import json

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql.functions import col, explode
from pyspark.sql.types import StructType

from src.infrastructure.logs import Logger


def read_spark_from_json(path: str = "evidently_monitoring.json") -> DataFrame:
    """Transforms a JSON into Spark DataFrame

    Args:
        path (str, optional): path of the SJON file. Defaults to "evidently_monitoring.json".

    Returns:
        DataFrame: Spark DataFrame
    """
    logger = Logger().get_logger()
    logger.info("Loading Spark DataFrame from JSON")
    spark = SparkSession.builder.getOrCreate()
    sc = spark.sparkContext  # noqa
    return spark.read.json(path, multiLine=True)


def read_json(path: str = "evidently_monitoring.json") -> dict:
    """Load JSON file

    Args:
        path (str, optional): path of the SJON file. Defaults to "evidently_monitoring.json".

    Returns:
        dict: dictionary containing the JSON
    """
    with open(path, "r") as f:
        data = json.load(f)
    return data


def selected_accurate_drift_monitoring_data(
    parameters_selection: list[str], monitoring_output: dict = read_json()
) -> list[tuple[str]]:
    """Keep only useful information from the drift monitoring JSON output

    Args:
        parameters_selection (list[str]): list of selected parameters.
        monitoring_output (dict, optional): original JSON output from the evidently drif report. Defaults to read_json().

    Returns:
        list[tuple[str]]: timestamps, column_name, column_type, stattest_name, stattest_threshold, drift_score, drift_detected
    """
    logger = Logger().get_logger()
    logger.info(f"Keep only useful information from the drift monitoring JSON output: {parameters_selection}")

    metrics_output = monitoring_output["suite"]["metric_results"][0]["drift_by_columns"]
    parameters_list = []
    for v in metrics_output.values():
        parameters_list.append(tuple(map(v.get, parameters_selection)))
    return [(monitoring_output["timestamp"], parameters_list)]


def transform_monitoring_in_spark_dataframe(
    data: list[tuple[str]], schema: StructType, parameters_selection: list[str]
) -> DataFrame:
    """Load monitoring selected data into a Spark DataFrame

    Args:
        data (list[tuple[str]]): data to transform
        schema (StructType): type of each column that will be inferred from data
        parameters_selection (list[str]): list of selected parameters

    Returns:
        DataFrame: Spark DataFrame
    """
    spark = SparkSession.builder.getOrCreate()
    df = spark.createDataFrame(data=data, schema=schema)
    df = df.select("timestamp", explode("metrics").alias("metrics_exploded"))
    df = df.select([col("timestamp")] + [col(f"metrics_exploded.{i}").alias(f"{i}") for i in parameters_selection])
    return df
