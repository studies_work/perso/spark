from pyspark.sql import SparkSession
from pyspark.sql.functions import rand

spark = SparkSession.builder.getOrCreate()


def spark_data_generation(n_rows=3000, n_columns=5):
    return spark.range(n_rows).select(*[rand().alias(str(i)) for i in range(n_columns)])
