from evidently import ColumnMapping
from evidently.metrics import DataDriftTable
from evidently.report import Report
from evidently.spark.engine import SparkEngine

from src.domain.spark_data import spark_data_generation

reference = spark_data_generation()
current = spark_data_generation()

spark_drift_report = Report(metrics=[DataDriftTable()])

spark_drift_report.run(
    reference_data=reference,
    current_data=current,
    column_mapping=ColumnMapping(),
    engine=SparkEngine,
)

spark_drift_report.save_html("evidently_monitoring.html")
spark_drift_report.save("evidently_monitoring.json")
