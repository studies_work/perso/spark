import logging
import logging.config
from dataclasses import dataclass, field

from src.infrastructure.read_conf import get_conf


@dataclass
class Logger:
    config_dict: dict = field(default_factory=lambda: get_conf(conf_file_name="log_conf"))

    def get_logger(self, logger_name: str = "applicatif") -> logging.Logger:
        """Logs configuration

        Args:
            logger_name (str, optional): name of the logger. Defaults to "applicatif".
            config (dict, optional): configuration dictionnary. Defaults to get_conf(conf_file_name="log_conf").

        Raises:
            ValueError: Returns error if logger name not validated

        Returns:
            logging.Logger: A logger instance
        """
        if self.config_dict is None:
            raise ValueError("Config dictionionary is empty")
        logger_names = ["", "applicatif"]
        if logger_name not in logger_names:
            raise ValueError(f"Invalid logger_name type. Expected one of: {logger_names}")
        else:
            logging.config.dictConfig(config=self.config_dict)
            logger = logging.getLogger(logger_name)
        return logger
