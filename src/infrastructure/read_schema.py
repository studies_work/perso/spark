from pyspark.sql.types import ArrayType, BooleanType, DoubleType, FloatType, StringType, StructField, StructType


def read_schema_for_selected_parameters(parameters_selection_schema: dict) -> StructType:
    """Create modular schema depending on the informations from the evidently JSON output needed

    Args:
        parameters_selection_schema (dict): dictionnary containing the elements to display into the final CSV report

    Returns:
        StructType: Schema structure for a Saprk DataFrame
    """
    pyspark_sql_types = {
        "StringType": StringType(),
        "FloatType": FloatType(),
        "DoubleType": DoubleType(),
        "BooleanType": BooleanType(),
    }
    schema = StructType()
    for k, v in parameters_selection_schema.items():
        schema.add(k, pyspark_sql_types.get(v), True)

    schema = StructType(
        [
            StructField("timestamp", StringType(), True),
            StructField("metrics", ArrayType(schema), True),
        ]
    )
    return schema
