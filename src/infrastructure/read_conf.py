import os
import pkgutil

import yaml


def get_conf(
    conf_file_name: str,
    conf_folder_name: str = "config",
    module_path: str = "src.infrastructure",
) -> dict:
    """Get configuration dictionnary from pkgutil

    Args:
        conf_file_name (str): name of the configuration file
        conf_folder_name (str, optional): name of the configuration folder. Defaults to "config".
        module_path (str, optional): path of the python package where the configuration folder is located. Defaults to "src.infrastructure".

    Returns:
        dict: configuration dictionnary
    """
    try:
        config = pkgutil.get_data(module_path, f"{conf_folder_name}/{conf_file_name}.yaml")
        config = yaml.safe_load(config)
        return config
    except FileNotFoundError as e:
        print(f"Path error: {e}")
        return None


def get_conf_with_yaml(
    conf_file_name: str,
    conf_folder_path: str = "src/infrastructure/config",
) -> dict:
    """Get configuration dictionnary from yaml

    Args:
        conf_file_name (str): name of the configuration file
        conf_folder_path (str, optional): path of the configuration folder. Defaults to "src/infrastructure/config".

    Returns:
        dict: configuration dictionnary
    """
    filepath = os.path.join(conf_folder_path, f"{conf_file_name}.yaml")
    try:
        with open(filepath, "r") as f:
            config = yaml.safe_load(f.read())
        return config
    except FileNotFoundError as e:
        print(f"Path error: {e}")
        return None
