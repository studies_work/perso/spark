from dataclasses import dataclass, field

from pyspark.sql import DataFrame

from src.domain.analyse_report import selected_accurate_drift_monitoring_data, transform_monitoring_in_spark_dataframe
from src.infrastructure.read_conf import get_conf
from src.infrastructure.read_schema import read_schema_for_selected_parameters


@dataclass
class DriftMonitoring:
    app_conf: dict = field(default_factory=lambda: get_conf(conf_file_name="app_conf"))

    def run_drift_monitoring(self) -> DataFrame:
        """Run Drift Monitoring

        Returns:
            DataFrame: Spark DataFrame
        """
        PARAMETERS_SELECTION = self.app_conf["monitoring"]["parameters_selection"]
        drift_monitoring_data = selected_accurate_drift_monitoring_data(
            parameters_selection=PARAMETERS_SELECTION.keys()
        )
        schema = read_schema_for_selected_parameters(parameters_selection_schema=PARAMETERS_SELECTION)
        dataframe = transform_monitoring_in_spark_dataframe(
            data=drift_monitoring_data, schema=schema, parameters_selection=PARAMETERS_SELECTION.keys()
        )
        dataframe.show(truncate=False)
        return dataframe


DriftMonitoring().run_drift_monitoring()
